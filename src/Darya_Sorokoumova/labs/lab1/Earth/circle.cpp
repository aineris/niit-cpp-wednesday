#include "circle.h"

Circle::Circle()
{
    radius = 0;
    ference= 0;
    area = 0;
}

double Circle::get_area() const
{
    return area;
}

double Circle::get_ference() const
{
    return ference;
}

double Circle::get_radius() const
{
    return radius;
}

bool Circle::set_radius(double r)
{
    if (check_data(r))
    {
        radius = r;
        ference = -1;
        area = -1;
        recalculate();
        return true;
    }
    else
        return false;
}

bool Circle::set_ference(double f)
{
    if (check_data(f))
    {
        ference = f;
        area = -1;
        radius = -1;
        recalculate();
        return true;
    }
    else
        return false;
}


bool Circle::set_area(double a)
{
    if (check_data(a))
    {
        area = a;
        radius = -1;
        ference = -1;
        recalculate();
        return true;
    }
    else
        return false;
}

bool Circle::check_data(double data)
{
    if ((data < MIN_VAL)||(data > MAX_VAL))
    {
        return false;
    }
    else
        return true;
}

void Circle::recalculate()
{
    if (radius != -1)
    {
        //changed radius
        ference = 2*M_PI*radius;
        area = M_PI*radius*radius;
    }
    else if (ference != -1)
    {
        //changed ference
        radius = ference/(2*M_PI);
        area = M_PI*radius*radius;
    }
    else if (area != -1)
    {
        //changed ference
        radius = sqrt(area/M_PI);
        ference = 2*M_PI*radius;
    }
}
