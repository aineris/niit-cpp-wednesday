#-------------------------------------------------
#
# Project created by QtCreator 2016-04-06T18:49:03
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Earth_demo
TEMPLATE = app


SOURCES += main.cpp\
        earth_demo.cpp \
    ../Circle/circle.cpp \
    earth.cpp


HEADERS  += earth_demo.h \
    earth.h \
    ../Circle/circle.h

FORMS    += earth_demo.ui
