#include "earth_demo.h"
#include "ui_earth_demo.h"
#include <QMessageBox>

#define prepare_number(x) (QString::number(x, 'g', 10))

Earth_demo::Earth_demo(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Earth_demo)
{
    ui->setupUi(this);
    ui->le_earth->setText(prepare_number(plt.get_radius()));
    rope.set_radius(EARTH_R);
}

Earth_demo::~Earth_demo()
{
    delete ui;
}

void Earth_demo::on_pb_calculate_clicked()
{
    double dL = -1;

    dL = (ui->le_new_length->text()).toDouble();

    if (dL >= 0)
    {
        calculate_new_data(dL);
    }
}

void Earth_demo::calculate_new_data(double dLen)
{
    if (rope.set_ference(plt.get_ference() + dLen))
    {
        //update fields

        double diffArea = rope.get_area() - plt.get_area();
        double diffRadius = rope.get_radius() - plt.get_radius();

        ui->le_new_area->setText(prepare_number(rope.get_area() - plt.get_area()));
        ui->le_new_distance->setText(prepare_number(rope.get_radius() - plt.get_radius()));
    }
}
