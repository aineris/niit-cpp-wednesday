#ifndef EARTH_H
#define EARTH_H

#include "circle.h"

#define EARTH_R 6378100

class Earth : public Circle
{
public:
    Earth();
};

#endif // EARTH_H
