#ifndef CIRCLE_H
#define CIRCLE_H

#include <math.h>

#define MIN_VAL 0
#define MAX_VAL 100000000000000000

class Circle
{
private:
    double radius;
    double ference;
    double area;

    void recalculate ();

public:
    Circle();
    double get_radius() const;
    double get_ference() const;
    double get_area() const;

    bool set_radius (double r);
    bool set_ference (double f);
    bool set_area (double a);

    bool check_data (double data);
};

#endif // CIRCLE_H
