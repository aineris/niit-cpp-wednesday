#ifndef EARTH_DEMO_H
#define EARTH_DEMO_H

#include <QMainWindow>
#include "earth.h"


namespace Ui {
class Earth_demo;
}

class Earth_demo : public QMainWindow
{
    Q_OBJECT
    Earth plt;
    Circle rope;


public:
    explicit Earth_demo(QWidget *parent = 0);
    ~Earth_demo();

    void calculate_new_data(double);

private slots:
    void on_pb_calculate_clicked();

private:
    Ui::Earth_demo *ui;
};

#endif // EARTH_DEMO_H
