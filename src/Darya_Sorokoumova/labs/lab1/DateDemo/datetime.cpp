#include "datetime.h"

DateTime::DateTime()
{
    time_k = time(0);
}

DateTime::DateTime(short d, short m, short y,
                   short h, short min, short s)
{
    time_k = time(0);

   // check_and_correct_date(y, m, d, h, min, s);

    struct tm * time_info = localtime((time_t*)&time_k);
    if (time_info != NULL)
    {
        time_info->tm_hour = h;
        time_info->tm_min = min;
        time_info->tm_sec = s;
        time_info->tm_year = y;
        time_info->tm_mon = m;
        time_info->tm_mday = d;

        time_k = mktime(time_info);
        if (time_k < 0)
            time_k = time(0);
    }
}

DateTime::DateTime(const DateTime & tm_c)
{
    if(tm_c.get_time_int() > 0)
        this->set_time_int(tm_c.get_time_int());
    else
        this->time_k = time(0);
}

DateTime DateTime::operator+ (short i)
{
    struct tm * time_info;

    time_info = localtime((time_t *)&time_k);
    if (time_info != NULL)
    {
        short day = (time_info->tm_mday + i);
        short mon = time_info->tm_mon;
        short year = time_info->tm_year;

        change_date_by_day(day, mon, year);

        time_info->tm_mday = day;
        time_info->tm_mon = mon;
        time_info->tm_year = year;

        int tmp_time = mktime(time_info);
        if (tmp_time > 0)
            time_k = tmp_time;
    }
    return *this;
}

DateTime DateTime::operator- (short i)
{
    struct tm * time_info;

    time_info = localtime((time_t *)&time_k);
    if (time_info != NULL)
    {
        short day = (time_info->tm_mday - i);
        short mon = time_info->tm_mon;
        short year = time_info->tm_year;

        change_date_by_day(day, mon, year);

        time_info->tm_mday = day;
        time_info->tm_mon = mon;
        time_info->tm_year = year;

        int tmp_time = mktime(time_info);
        if (tmp_time > 0)
            time_k = tmp_time;
    }
    return *this;
}

int DateTime::operator- (const DateTime& i)
{
    struct tm *time_info = NULL;
    int diff = 0;
    int time_f = i.get_time_int();

    time_info = localtime((const time_t*)&time_k);

    short to_ydays = time_info->tm_yday;
    short to_years = time_info->tm_year;


    time_info = localtime((const time_t*)&time_f);

    diff = to_ydays - time_info->tm_yday +
            (to_years - time_info->tm_year)*DAYS_IN_YEAR;

    return diff;
}

DateTime& DateTime::operator ++(int)
{
    this->operator +(1);
    return *this;
}

DateTime& DateTime::operator --(int)
{
    this->operator -(1);
    return *this;
}

int DateTime::get_time_int() const
{
    return time_k;
}

void DateTime::set_time_int(int val)
{
    if (val > 0)
        time_k = val;
}

string DateTime::get_today_str() const
{
    struct tm * time_info;
    string time_str;

    time_info = localtime((time_t *)&time_k);

    if (time_info != NULL)
    {
        time_str = to_string(time_info->tm_hour) + ':' + to_string(time_info->tm_min) + ':' +
                   to_string(time_info->tm_sec) + " " + get_day_name(time_info->tm_wday + 1) + " " +
                   to_string(time_info->tm_mday) + " " +  get_month_name(time_info->tm_mon + 1) + " " +
                   to_string(FIRST_YEAR + time_info->tm_year);

        return time_str;
    }
    else
        return NO_DATE;
}

string DateTime::get_yesterday_str() const
{
    DateTime tmp = DateTime(*this);
    tmp--;
    return (tmp.get_today_str());
}

string DateTime::get_tomorrow_str() const
{
    DateTime tmp = DateTime();
    tmp++;
    return (tmp.get_today_str());
}

string DateTime::get_future_str(int N) const
{
    DateTime tmp;
    tmp = tmp + N;
    return tmp.get_today_str();
}

string DateTime::get_past_str(int N) const
{
    DateTime tmp;
    tmp = tmp - N;
    return tmp.get_today_str();
}

string DateTime::get_month_name(short m) const
{
    if ( m < 0)
        return NO_DATE;

    m = (m == 0) ? get_current_month() : (m - 1);
    return months[m];
}

string DateTime::get_day_name(short d) const
{
    if (d < 0)
        return NO_DATE;

    d = (d == 0) ? get_current_day() : (d - 1);
    return days[d];
}

void DateTime::check_and_correct_date(short& y, short& m, short& d,
                                      short& h, short& min, short& s)
{
    if (y < 0)
        y = 0;

    if ((m < 0)||(m >= MONTHS_IN_YEAR))
        m = 0;

    if ((d < 0)||(d > 7))
        d = 0;

    if ((h < 0)||(h > 24))
        h = 0;

    if ((min < 0)||(min > 59))
        min = 0;

    if ((s < 0) || (s > 59))
        s = 0;
    return;
}

short DateTime::get_current_month() const
{
    struct tm * time_info;

    time_info = localtime((time_t *)&time_k);

    if (time_info)
        return (short)time_info->tm_mon;
    else
        return 0;
}

short DateTime::get_current_day() const
{
    struct tm * time_info;

    time_info = localtime((time_t *)&time_k);

    if (time_info)
        return (short)time_info->tm_mday;
    else
        return 0;
}

short DateTime::get_days_in_month(short mon, short year)
{
    if (mon < 7)
    {
        if (mon == 1)
        {
            if (leap_year(year))
                return FEB_V;
            else
                return FEB_ORD;
        }
        else
            return((mon % 2 == 0) ? DAYS_31:DAYS_30);
    }
    else
       return((mon % 2 != 0) ? DAYS_31:DAYS_30);
}

void DateTime::change_date_by_day(short& day, short& mon, short& year)
{
        short days_in_month = get_days_in_month(mon, year);
        if (day > days_in_month)
        {
            //increment month
            mon++;
            day-= days_in_month;
            if (mon > MONTHS_IN_YEAR)
            {
                mon = 0;
                year++;
            }
            if (day > days_in_month)
                change_date_by_day(day, mon, year);
        }
        else if (day < 0)
        {
            //decrement month
            mon--;
            days_in_month = get_days_in_month(mon, year);
            day+= days_in_month+1;
            if (mon < 0)
            {
                mon = 11;
                year--;
            }
            if (day < 0)
                change_date_by_day(day, mon, year);
        }
}

int DateTime::calculate_difference_days(const DateTime& date_from)
{
    return (this->operator -(date_from));
}

bool DateTime::leap_year(short year)
{
    if (((year % 4 == 0)&&(year % 100 != 0)) || (year % 400 == 0))
        return true;
    else
        return false;
}
