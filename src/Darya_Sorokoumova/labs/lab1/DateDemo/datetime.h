#ifndef DATETIME_H
#define DATETIME_H

#include "time.h"
#include <string>

using namespace std;

/*
 * Known limittions:
 * 1) Assume that every year has 365 days - no 366 day years.
 * 2) Assume that february always has 28 days - no 29 days.
*/

#define NO_DATE "Date is not available"

#define DAYS_IN_YEAR 365
#define FIRST_YEAR 1900
#define MONTHS_IN_YEAR 12

enum days_mon {
    FEB_ORD = 28,
    FEB_V   = 29,
    DAYS_30 = 30,
    DAYS_31 = 31
};

const string days[] = {"Sunday", "Monday", "Tuesday", "Wendnesday",
                "Thursday", "Friday", "Satturday"};

const string months[] = {"January", "February", "March",
                         "April", "May", "June",
                         "July", "August", "September",
                         "October", "November", "December"};

class DateTime
{
private:
    int time_k;

    int get_time_int() const;
    void set_time_int(int val);

    short get_current_month() const;
    short get_current_day() const;

    bool leap_year(short year);
    short get_days_in_month(short mon, short year);

    void change_date_by_day(short& day, short& mon, short& year);

    void check_and_correct_date(short& y, short& m, short& d,
                                short& h, short& min, short& s);

public:
    DateTime();
    DateTime(short d, short m, short y, short h = 0, short min = 0, short s = 0);
    DateTime(const DateTime &tm_c);

    // Operators
    DateTime operator+ (short i);
    DateTime operator- (short i);
    DateTime &operator ++(int);
    DateTime &operator --(int);

    int operator- (const DateTime& i); //To calc diff in days

    // Several methods for printing
    string get_today_str() const;
    string get_yesterday_str() const;
    string get_tomorrow_str() const;
    string get_future_str(int N) const;
    string get_past_str(int N) const;

    // If the arg is 0, methods returns current month/day name
    string get_month_name(short m = 0) const;
    string get_day_name(short d = 0) const;

    /*
     * To date - is the calling object, from date - arguement.
     * Returns difference in days.
     */
    int calculate_difference_days(const DateTime& date_from);
};

#endif // DATETIME_H
