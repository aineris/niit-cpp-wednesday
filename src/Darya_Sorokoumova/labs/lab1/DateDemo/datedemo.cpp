#include "datedemo.h"
#include "ui_datedemo.h"
#include <QDate>

DateDemo::DateDemo(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::DateDemo)
{
    ui->setupUi(this);
    ui->te_display_time->setText(QString::fromStdString(date_main.get_today_str()));
    ui->te_display_time_f_p->setText(QString::fromStdString(date_main.get_today_str()));

    ui->de_from->setDate(QDate::currentDate());
    ui->de_to->setDate(QDate::currentDate());

}

DateDemo::~DateDemo()
{
    delete ui;
}

void DateDemo::on_pb_yesterday_clicked()
{
    ui->te_display_time->setText(QString::fromStdString(date_main.get_yesterday_str()));
}

void DateDemo::on_pb_tomorrow_clicked()
{
    ui->te_display_time->setText(QString::fromStdString(date_main.get_tomorrow_str()));
}

void DateDemo::on_pb_current_clicked()
{
    ui->te_display_time->setText(QString::fromStdString(date_main.get_today_str()));
}

void DateDemo::on_pb_future_clicked()
{
    short dH = 0;
    dH = ui->sb_days_N->text().toShort();

    if (dH > 0)
    {
        ui->te_display_time_f_p->setText(QString::fromStdString(date_main.get_future_str(dH)));
    }
}

void DateDemo::on_pb_past_clicked()
{
    short dH = 0;
    dH = ui->sb_days_N->text().toShort();

    if (dH > 0)
    {
        ui->te_display_time_f_p->setText(QString::fromStdString(date_main.get_past_str(dH)));
    }
}

void DateDemo::on_pb_past_2_clicked()
{
    QDate d1, d2;

    d1 = ui->de_from->date();
    d2 = ui->de_to->date();

    DateTime _d1(d1.day() - 1, d1.month() - 1, (d1.year() - 1900));
    DateTime _d2(d2.day() - 1, d2.month() - 1, (d2.year() - 1900));

    int diff = _d2.calculate_difference_days(_d1);

    ui->lcdNumber->display(diff);
}
