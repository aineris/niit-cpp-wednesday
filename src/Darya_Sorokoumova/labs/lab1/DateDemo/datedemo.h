#ifndef DATEDEMO_H
#define DATEDEMO_H

#include "datetime.h"
#include <QMainWindow>
#include <QString>

using namespace std;

namespace Ui {
class DateDemo;
}

class DateDemo : public QMainWindow
{
    Q_OBJECT

public:
    explicit DateDemo(QWidget *parent = 0);
    ~DateDemo();

private slots:
    void on_pb_yesterday_clicked();

    void on_pb_tomorrow_clicked();

    void on_pb_current_clicked();

    void on_pb_future_clicked();

    void on_pb_past_clicked();

    void on_pb_past_2_clicked();

private:
    Ui::DateDemo *ui;
    DateTime date_main;
    DateTime date_diff;
};

#endif // DATEDEMO_H
