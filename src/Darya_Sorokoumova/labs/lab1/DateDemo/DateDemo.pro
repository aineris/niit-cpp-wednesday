#-------------------------------------------------
#
# Project created by QtCreator 2016-04-18T15:30:17
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DateDemo
TEMPLATE = app


SOURCES += main.cpp\
        datedemo.cpp \
    datetime.cpp

HEADERS  += datedemo.h \
    datetime.h

FORMS    += datedemo.ui
