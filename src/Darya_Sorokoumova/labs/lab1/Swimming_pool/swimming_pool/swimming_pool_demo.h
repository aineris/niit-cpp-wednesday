#ifndef SWIMMING_POOL_DEMO_H
#define SWIMMING_POOL_DEMO_H

#include <QMainWindow>
#include "pool.h"

namespace Ui {
class Swimming_pool_demo;
}

class Swimming_pool_demo : public QMainWindow
{
    Q_OBJECT
    Pool pool;

public:
    explicit Swimming_pool_demo(QWidget *parent = 0);
    ~Swimming_pool_demo();

    show_default_values();

private slots:
    void on_pb_clear_clicked();

    void on_pb_calculate_clicked();

private:
    Ui::Swimming_pool_demo *ui;
};

#endif // SWIMMING_POOL_DEMO_H
