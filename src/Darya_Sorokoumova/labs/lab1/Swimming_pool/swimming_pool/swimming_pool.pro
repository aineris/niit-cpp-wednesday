#-------------------------------------------------
#
# Project created by QtCreator 2016-04-12T01:23:13
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = swimming_pool
TEMPLATE = app


SOURCES += main.cpp\
        swimming_pool_demo.cpp \
    ../../Circle/circle.cpp \
    pool.cpp

HEADERS  += swimming_pool_demo.h \
    ../../Circle/circle.h \
    pool.h

FORMS    += swimming_pool_demo.ui
