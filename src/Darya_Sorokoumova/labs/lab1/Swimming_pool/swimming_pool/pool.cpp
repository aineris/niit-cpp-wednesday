#include "pool.h"

Pool::Pool()
{
    set_radius(POOL_R);
    fence.set_radius(POOL_R + PATH_WIDTH);
}

bool Pool::set_fence_params(double w)
{
    return fence.set_radius(get_radius() + w);
}

double Pool::get_fence_ference() const
{
    return fence.get_ference();
}

double Pool::get_fence_area() const
{
    return fence.get_area();
}

double Pool::get_fence_radius() const
{
    return fence.get_radius();
}
