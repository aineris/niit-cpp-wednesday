#ifndef POOL_H
#define POOL_H

#include "../../Circle/circle.h"

#define PRICE_PATH 1000
#define PRICE_FENCE 2000
#define POOL_R 3
#define PATH_WIDTH 1

class Pool : public Circle
{
private:
    Circle fence;

public:
    Pool();
    bool set_fence_params(double w);
    double get_fence_ference() const;
    double get_fence_area() const;
    double get_fence_radius() const;
};

#endif // POOL_H
