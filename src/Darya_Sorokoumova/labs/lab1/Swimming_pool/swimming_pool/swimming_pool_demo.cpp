#include "swimming_pool_demo.h"
#include "ui_swimming_pool_demo.h"

#define prepare_number(x) (QString::number(x, 'g', 8))

Swimming_pool_demo::Swimming_pool_demo(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Swimming_pool_demo)
{
    ui->setupUi(this);
    show_default_values();
}

Swimming_pool_demo::~Swimming_pool_demo()
{
    delete ui;
}

Swimming_pool_demo::show_default_values()
{
    ui->le_pool_radius->setText(prepare_number(pool.get_radius()));
    ui->le_price_path->setText(prepare_number(PRICE_PATH));
    ui->le_price_fe->setText(prepare_number(PRICE_FENCE));
    ui->le_width_path->setText(prepare_number(PATH_WIDTH));

    //Clear calculated lines
    ui->le_fence_cost->clear();
    ui->le_path_cost->clear();
}

void Swimming_pool_demo::on_pb_clear_clicked()
{
    show_default_values();
}

void Swimming_pool_demo::on_pb_calculate_clicked()
{
    bool translate_correct = false;
    double path_price = -1;
    double fence_price = -1;
    double fence_cost = -1;
    double path_cost = -1;

    path_price = (ui->le_price_path->text()).toDouble(&translate_correct);
    fence_price = (ui->le_price_fe->text()).toDouble(&translate_correct);

    if (pool.set_radius((ui->le_pool_radius->text()).toDouble(&translate_correct)) &&
            pool.set_fence_params((ui->le_width_path->text()).toDouble(&translate_correct)))
    {
        //everything looks correct, calculate costs
        fence_cost = pool.get_fence_ference() * fence_price;
        path_cost = (pool.get_fence_area() - pool.get_area()) * path_price;

        if ((fence_cost >= 0) && (path_cost >= 0))
        {
            //update fields
            ui->le_path_cost->setText(prepare_number(path_cost));
            ui->le_fence_cost->setText(prepare_number(fence_cost));
        }
    }
}
