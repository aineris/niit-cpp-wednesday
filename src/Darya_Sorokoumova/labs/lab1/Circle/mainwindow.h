#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include "circle.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    Circle figure;

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pb_radius_clicked();

    void on_pb_ference_clicked();

    void on_pb_area_clicked();

    void on_le_radius_textChanged(const QString &arg1);

    void on_le_radius_editingFinished();

    void on_le_ference_editingFinished();

    void on_le_area_editingFinished();

    void enableItems();

    void on_le_ference_textChanged(const QString &arg1);

    void on_le_area_textChanged(const QString &arg1);

    void showError();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
