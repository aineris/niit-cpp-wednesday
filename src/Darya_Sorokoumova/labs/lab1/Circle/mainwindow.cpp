#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->le_radius->setText("0");
    ui->le_ference->setText("0");
    ui->le_area->setText("0");

    enableItems();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pb_radius_clicked()
{
    QString r = ui->le_radius->text();
    bool translate_res = false;

    if (figure.set_radius(r.toDouble(&translate_res)) && translate_res)
    {
        ui->le_ference->setText(QString::number(figure.get_ference()));
        ui->le_area->setText(QString::number(figure.get_area()));

        enableItems();
    }
    else
        showError();
}

void MainWindow::on_pb_ference_clicked()
{
    QString f = ui->le_ference->text();
    bool translate_res = false;

    if (figure.set_ference(f.toDouble(&translate_res)) && translate_res)
    {
        ui->le_radius->setText(QString::number(figure.get_radius()));
        ui->le_area->setText(QString::number(figure.get_area()));

        enableItems();
    }
    else
        showError();
}

void MainWindow::on_pb_area_clicked()
{
    QString a = ui->le_area->text();
    bool translate_res = false;

    if (figure.set_area(a.toDouble(&translate_res)) && translate_res)
    {
        ui->le_ference->setText(QString::number(figure.get_ference()));
        ui->le_radius->setText(QString::number(figure.get_radius()));

        enableItems();
    }
    else
        showError();
}

void MainWindow::on_le_radius_textChanged(const QString &arg1)
{
    on_le_radius_editingFinished();
}

void MainWindow::on_le_radius_editingFinished()
{
    if (ui->le_radius->isEnabled())
    {
        //Allow to press button
        ui->pb_radius->setEnabled(1);

        //Disallow to change other fields or press buttons
        ui->pb_ference->setDisabled(1);
        ui->pb_area->setDisabled(1);
        ui->le_area->setDisabled(1);
        ui->le_ference->setDisabled(1);
    }
}

void MainWindow::on_le_ference_editingFinished()
{
    if (ui->le_ference->isEnabled())
    {
        //Allow to press button
        ui->pb_ference->setEnabled(1);

        //Disallow to change other fields or press buttons
        ui->pb_radius->setDisabled(1);
        ui->pb_area->setDisabled(1);
        ui->le_area->setDisabled(1);
        ui->le_radius->setDisabled(1);
    }

}

void MainWindow::on_le_area_editingFinished()
{
    if (ui->le_area->isEnabled())
    {
        //Allow to press button
        ui->pb_area->setEnabled(1);

        //Disallow to change other fields or press buttons
        ui->pb_ference->setDisabled(1);
        ui->pb_radius->setDisabled(1);
        ui->le_ference->setDisabled(1);
        ui->le_radius->setDisabled(1);
    }
}

void MainWindow::enableItems()
{
    ui->le_area->setDisabled(0);
    ui->le_ference->setDisabled(0);
    ui->le_radius->setDisabled(0);

    ui->pb_area->setDisabled(0);
    ui->pb_ference->setDisabled(0);
    ui->pb_radius->setDisabled(0);
}

void MainWindow::on_le_ference_textChanged(const QString &arg1)
{
    on_le_ference_editingFinished();
}

void MainWindow::on_le_area_textChanged(const QString &arg1)
{
    on_le_area_editingFinished();
}

void MainWindow::showError()
{
    QMessageBox err_msg(this);

    err_msg.setWindowTitle("Error");

    err_msg.setText("Error: Radius, Ference and Area data must be in range of (0-1000)!");
    err_msg.exec();
}
